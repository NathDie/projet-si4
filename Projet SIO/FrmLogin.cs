﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projet_SIO
{
    public partial class FrmLogin : Form
    {
        public Joueur Joueur { get; set; }

        public FrmLogin()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void login_Load(object sender, EventArgs e)
        {

        }

        private void connexion_Click(object sender, EventArgs e)
        {
            MySqlConnectionStringBuilder stringBuilder = new MySqlConnectionStringBuilder();
            stringBuilder.Server = "localhost";
            stringBuilder.UserID = "root";
            stringBuilder.Password = "";
            stringBuilder.Database = "projetbiere";
            stringBuilder.SslMode = MySqlSslMode.None;
            stringBuilder.Port = 3306;

            var connString = stringBuilder.ToString();
            
            string sql = "select * from joueur where login = ?users and pwd = ?password;";

            MySqlConnection conn = new MySqlConnection(connString);
            conn.Open();

            MySqlCommand cmd = new MySqlCommand(sql, conn);
            cmd.Parameters.Add(new MySqlParameter("users", users.Text));
            cmd.Parameters.Add(new MySqlParameter("password", password.Text));

            MySqlDataReader rdr = cmd.ExecuteReader();
            bool resultat = rdr.Read();
            if (resultat)
            {
                MessageBox.Show("Soyez la bienvenue sur cette application");
                Joueur = new Joueur();
                Joueur.id = rdr.GetInt32("id");
                Joueur.login = rdr.GetString("login");
                Joueur.pwd = rdr.GetInt32("pwd");
                rdr.Close();
            }
            else
            {
                MessageBox.Show("Erreur de connection, les informations que vous venez de saisir ne sont pas conforme, veuillez réésayer");
            }

            conn.Close();


            if (resultat)
            {
                
                DialogResult = DialogResult.OK;
            }
        }
    }
}

