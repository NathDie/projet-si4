﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Projet_SIO
{
    public partial class FrmResultat : Form
    {
        public FrmResultat()
        {
            InitializeComponent();
        }

        private void rquitter_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void resultat_Load(object sender, EventArgs e)
        {
            loadgrid();
        }

        private void loadgrid()
        {
            MySqlConnectionStringBuilder stringBuilder = new MySqlConnectionStringBuilder();
            stringBuilder.Server = "localhost";
            stringBuilder.UserID = "root";
            stringBuilder.Password = "";
            stringBuilder.Database = "projetbiere";
            stringBuilder.SslMode = MySqlSslMode.None;
            stringBuilder.Port = 3306;

            var connString = stringBuilder.ToString();
            string sql = "SELECT biere, note from resultat";
            MySqlConnection conn = new MySqlConnection(connString);
            conn.Open();

            MySqlCommand cmd = new MySqlCommand(sql, conn);
            MySqlDataReader rdr = cmd.ExecuteReader();

            dataGridView1.Rows.Clear();


            if (rdr.HasRows)
            {
                while (rdr.Read())
                {
                    dataGridView1.Rows.Add(rdr[0].ToString(), rdr[1].ToString());
                }
            }
            rdr.Close();
            conn.Close();


        }
    }
}
