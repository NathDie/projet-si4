﻿namespace Projet_SIO
{
    partial class FrmVote
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.resultat = new System.Windows.Forms.Button();
            this.valider = new System.Windows.Forms.Button();
            this.aide = new System.Windows.Forms.Button();
            this.nondefini = new System.Windows.Forms.Button();
            this.pictureboxsio = new System.Windows.Forms.PictureBox();
            this.note = new System.Windows.Forms.NumericUpDown();
            this.txtnombiere = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnresult = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureboxsio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.note)).BeginInit();
            this.SuspendLayout();
            // 
            // resultat
            // 
            this.resultat.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.resultat.Location = new System.Drawing.Point(12, 9);
            this.resultat.Name = "resultat";
            this.resultat.Size = new System.Drawing.Size(33, 20);
            this.resultat.TabIndex = 0;
            this.resultat.UseVisualStyleBackColor = true;
            this.resultat.Visible = false;
            this.resultat.Click += new System.EventHandler(this.button1_Click);
            // 
            // valider
            // 
            this.valider.Location = new System.Drawing.Point(129, 256);
            this.valider.Name = "valider";
            this.valider.Size = new System.Drawing.Size(81, 23);
            this.valider.TabIndex = 3;
            this.valider.Text = "Valider";
            this.valider.UseVisualStyleBackColor = true;
            this.valider.Click += new System.EventHandler(this.valider_Click);
            // 
            // aide
            // 
            this.aide.Location = new System.Drawing.Point(302, 5);
            this.aide.Name = "aide";
            this.aide.Size = new System.Drawing.Size(20, 20);
            this.aide.TabIndex = 4;
            this.aide.Text = "?";
            this.aide.UseVisualStyleBackColor = true;
            this.aide.Click += new System.EventHandler(this.aide_Click);
            // 
            // nondefini
            // 
            this.nondefini.Location = new System.Drawing.Point(258, 136);
            this.nondefini.Name = "nondefini";
            this.nondefini.Size = new System.Drawing.Size(33, 20);
            this.nondefini.TabIndex = 5;
            this.nondefini.Text = ">";
            this.nondefini.UseVisualStyleBackColor = true;
            this.nondefini.Click += new System.EventHandler(this.button1_Click);
            // 
            // pictureboxsio
            // 
            this.pictureboxsio.Image = global::Projet_SIO.Properties.Resources._1664;
            this.pictureboxsio.Location = new System.Drawing.Point(110, 70);
            this.pictureboxsio.Name = "pictureboxsio";
            this.pictureboxsio.Size = new System.Drawing.Size(118, 180);
            this.pictureboxsio.TabIndex = 1;
            this.pictureboxsio.TabStop = false;
            // 
            // note
            // 
            this.note.Location = new System.Drawing.Point(12, 259);
            this.note.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.note.Name = "note";
            this.note.Size = new System.Drawing.Size(100, 20);
            this.note.TabIndex = 6;
            // 
            // txtnombiere
            // 
            this.txtnombiere.Location = new System.Drawing.Point(225, 259);
            this.txtnombiere.Name = "txtnombiere";
            this.txtnombiere.Size = new System.Drawing.Size(97, 20);
            this.txtnombiere.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(79, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(212, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Avant de commercer à voter, cliquer ici !  ->";
            // 
            // btnresult
            // 
            this.btnresult.Location = new System.Drawing.Point(129, 41);
            this.btnresult.Name = "btnresult";
            this.btnresult.Size = new System.Drawing.Size(81, 23);
            this.btnresult.TabIndex = 9;
            this.btnresult.Text = "resultat";
            this.btnresult.UseVisualStyleBackColor = true;
            this.btnresult.Click += new System.EventHandler(this.btnresult_Click);
            // 
            // FrmVote
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(334, 290);
            this.Controls.Add(this.btnresult);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtnombiere);
            this.Controls.Add(this.note);
            this.Controls.Add(this.nondefini);
            this.Controls.Add(this.aide);
            this.Controls.Add(this.valider);
            this.Controls.Add(this.pictureboxsio);
            this.Controls.Add(this.resultat);
            this.Name = "FrmVote";
            this.ShowIcon = false;
            this.Text = "Projet SIO";
            this.Load += new System.EventHandler(this.fenetre1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureboxsio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.note)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button resultat;
        private System.Windows.Forms.PictureBox pictureboxsio;
        private System.Windows.Forms.Button valider;
        private System.Windows.Forms.Button aide;
        private System.Windows.Forms.Button nondefini;
        private System.Windows.Forms.NumericUpDown note;
        private System.Windows.Forms.TextBox txtnombiere;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnresult;
    }
}

