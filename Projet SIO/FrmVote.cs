﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projet_SIO
{
    public partial class FrmVote : Form
    {
        public Joueur Joueur { get; set; }

        public FrmVote()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DisplayNextFile(count);
        }


        string path = @"C:\Users\Sweety\source\repos\Projet SIO\Projet SIO\img";
        int count = 0;

        private void DisplayNextFile(int c)
        {
            var rand = new Random();
            var files = System.IO.Directory.GetFiles(path, "*.jpg");
            var images = new Image[files.Length];

            for (int i = c; i < files.Length;)
            {
                images[i] = Image.FromFile(files[i]);
                pictureboxsio.Image = images[i];
                break;
            }


            count++;

            if (count == files.Length)
                count = 0;
        }

        private void fenetre1_Load(object sender, EventArgs e)
        {
            DisplayNextFile(count);
            FrmLogin frm = new FrmLogin();
            DialogResult result = frm.ShowDialog();
            if (result != DialogResult.OK)
            {
                Application.Exit();
            }
            else
            {
                Joueur = frm.Joueur;
            }


        }

        private void aide_Click(object sender, EventArgs e)
        {
            FrmAide frm = new FrmAide();
            frm.ShowDialog();
        }

        private void valider_Click(object sender, EventArgs e)
        {

            MySqlConnectionStringBuilder stringBuilder = new MySqlConnectionStringBuilder();
            stringBuilder.Server = "localhost";
            stringBuilder.UserID = "root";
            stringBuilder.Password = "";
            stringBuilder.Database = "projetbiere";
            stringBuilder.SslMode = MySqlSslMode.None;
            stringBuilder.Port = 3306;

            var connString = stringBuilder.ToString();

            string sql = "INSERT INTO resultat (biere, note) VALUES (@nombiere, @note)";

            MySqlConnection conn = new MySqlConnection(connString);
            conn.Open();

            MySqlCommand cmd = new MySqlCommand(sql, conn);
            cmd.Parameters.Add(new MySqlParameter("@note", note.Value));
            cmd.Parameters.Add(new MySqlParameter("@nombiere", txtnombiere.Text));
            cmd.ExecuteNonQuery();

            MessageBox.Show("Votre vote a été ajouté !");

            conn.Close();

        }

        private void btnresult_Click(object sender, EventArgs e)
        {
            FrmResultat frm = new FrmResultat();
            frm.ShowDialog();
        }
    }
}
