﻿namespace Projet_SIO
{
    partial class FrmLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.users = new System.Windows.Forms.TextBox();
            this.userstext = new System.Windows.Forms.Label();
            this.passwordtext = new System.Windows.Forms.Label();
            this.password = new System.Windows.Forms.TextBox();
            this.connexion = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // users
            // 
            this.users.Location = new System.Drawing.Point(71, 36);
            this.users.Name = "users";
            this.users.Size = new System.Drawing.Size(130, 20);
            this.users.TabIndex = 0;
            // 
            // userstext
            // 
            this.userstext.AutoSize = true;
            this.userstext.Location = new System.Drawing.Point(96, 20);
            this.userstext.Name = "userstext";
            this.userstext.Size = new System.Drawing.Size(84, 13);
            this.userstext.TabIndex = 1;
            this.userstext.Text = "Nom d\'utilisateur";
            this.userstext.Click += new System.EventHandler(this.label1_Click);
            // 
            // passwordtext
            // 
            this.passwordtext.AutoSize = true;
            this.passwordtext.Location = new System.Drawing.Point(96, 90);
            this.passwordtext.Name = "passwordtext";
            this.passwordtext.Size = new System.Drawing.Size(71, 13);
            this.passwordtext.TabIndex = 2;
            this.passwordtext.Text = "Mot de passe";
            // 
            // password
            // 
            this.password.Location = new System.Drawing.Point(71, 108);
            this.password.Name = "password";
            this.password.Size = new System.Drawing.Size(130, 20);
            this.password.TabIndex = 3;
            this.password.UseSystemPasswordChar = true;
            // 
            // connexion
            // 
            this.connexion.Location = new System.Drawing.Point(99, 148);
            this.connexion.Name = "connexion";
            this.connexion.Size = new System.Drawing.Size(75, 23);
            this.connexion.TabIndex = 4;
            this.connexion.Text = "Connexion";
            this.connexion.UseVisualStyleBackColor = true;
            this.connexion.Click += new System.EventHandler(this.connexion_Click);
            // 
            // FrmLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(274, 196);
            this.Controls.Add(this.connexion);
            this.Controls.Add(this.password);
            this.Controls.Add(this.passwordtext);
            this.Controls.Add(this.userstext);
            this.Controls.Add(this.users);
            this.Name = "FrmLogin";
            this.ShowIcon = false;
            this.Text = "Login";
            this.Load += new System.EventHandler(this.login_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox users;
        private System.Windows.Forms.Label userstext;
        private System.Windows.Forms.Label passwordtext;
        private System.Windows.Forms.TextBox password;
        private System.Windows.Forms.Button connexion;
    }
}